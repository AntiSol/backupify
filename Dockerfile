FROM google/cloud-sdk:latest

RUN apt-get update && apt-get install -y pv && rm -rf /var/lib/apt/lists/*

RUN mkdir /gcloud
WORKDIR /gcloud
ADD ./run.sh /gcloud/run.sh

# setup happens in run script, we just need to pass vars through
CMD [ "./run.sh" ]

