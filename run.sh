#!/bin/bash
# Google Cloud Storage backup script
#  Creates a daily backup by compressing the provided directory and uploading it into your cloud bucket.
#
# expects that your json key file is mounted as /gcloud/key.json
# expects that you have mounted something to backup at /data
# expects a CLOUD_DEST arg / env var pointing to the destination directory

KEY_FILE=/gcloud/key.json

if [ ! -r "$KEY_FILE" ]; then 
	echo "ERROR: Keyfile '$KEY_FILE' does not exist or is not readable! Use '-v /path/to/keyfile.json:$KEY_FILE' in your docker run incantation"
	exit 1
fi

if [ ! -d /data ]; then
	echo "ERROR: You need to mount something to back up at /data. Use '-v /path/to/directory:/data' in your incantation"
	exit 1
fi

if [ -z "$CLOUD_DEST" ] || [ "$CLOUD_DEST" == unset ]; then
	echo "ERROR: You need to provide a CLOUD_DEST env var to specify the backup destination. Use '-e CLOUD_DEST=gs://your-bucket/path/' in your incantation"
	exit 1
fi

if [ -z "$BWLIMIT" ]; then
	# default bandwidth limit is (relatively) low, only 2 megs, to prevent causing problems during backup
	BWLIMIT="2M"
fi

# If anything below here fails, bail out immediately
set -e

# Aight, everything looks like it might be valid.

# set up gcloud with creds
#echo "Activating service account..."
gcloud auth activate-service-account --key-file "$KEY_FILE" >/dev/null 2>&1

destfile="backup_`date +%Y-%m-%d`.tar.bz2"

#echo "Performing backup..."
# Notes:
# - use nice to prevent maxing out cpu and slowing other things down
# - use pv to limit bandwidth
# useful gsutil options
# -q	quiet, only report failure
# -m	multithreaded, increases speed (not ideal, could cause an outage, we limit bw)
#nice tar jc -C /data/ . | pv -q -L "$BWLIMIT" | gsutil cp -L /gcloud/logfile - "$CLOUD_DEST/$destfile" >/dev/null 2>&1
nice tar jc -C /data/ . | pv -q -L "$BWLIMIT" | gcloud storage cp - "$CLOUD_DEST/$destfile"

